//
//  HydrateViewController.swift
//  Water Tracker
//
//  Created by Jordan Helzer on 5/8/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit

class HydrateViewController: UIViewController {

	@IBOutlet weak var fillView: FillView!
	
	var dailyGoal: Float = 75
	var amountDrank: Float = 0.0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		fillView.layer.borderColor = UIColor.black.cgColor
		fillView.layer.borderWidth = 1.0
		fillView.fill = amountDrank / dailyGoal
	}
	
	func addDrink(_ drank: Float) {
		amountDrank = amountDrank + drank
		self.fillView.fill = self.amountDrank / self.dailyGoal
	}

	@IBAction func addButtonPressed(_ sender: Any) {
		let amounts: [Float] = [8.0, 16.9, 24.0, 32.0]
		let alert = UIAlertController(title: "Add Some Drink", message: "Select the amount that you would like to add", preferredStyle: .actionSheet)
		
		for amount in amounts {
			alert.addAction(UIAlertAction(title: "\(String(format: "%.1f", amount)) Fl Oz", style: .default, handler: { (action) in
				self.addDrink(amount)
			}))
	
		} // End For
		
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		present(alert, animated: true, completion: nil)
	}

}

