//
//  FillView.swift
//  Water Tracker
//
//  Created by Jordan Helzer on 5/16/17.
//  Copyright © 2017 HelzApps. All rights reserved.
//

import UIKit

class FillView: UIView {

	fileprivate var _fill: Float = 0
	var fill: Float {
		get {
			return _fill
		}
		set {
			_fill = newValue
			if newValue > 1.0 {
				_fill = 1.0
			} else if newValue < 0.0 {
				_fill = 0.0
			}
			setNeedsDisplay()
		}
	}

	
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
		
		super.draw(rect)
		guard let context = UIGraphicsGetCurrentContext() else {
			return
		}
		
		let rectFrame = CGRect(x: 0, y: rect.height * CGFloat(1 - fill), width: frame.width, height: frame.height - (rect.height * CGFloat(1 - fill)))
		let color = UIColor(colorLiteralRed: 53.0/255.0, green: 157.0/255.0, blue: 224.0/255.0, alpha: 1.0)
		color.setFill()
		context.fill(rectFrame)
    }
	

}
